<?php

class MenuSetting extends CI_Model{

	public function __construct() {
        parent::__construct();
        
        $this->load->database();

        /* variable for result which return in controller */
        $this->response = array();
    }

    public function get_data() {
        $result = $this->db->get('api_menu_setting')->result();
        $this->response['code'] = 0;
        $this->response['msg']  = 'data setting menu';
        $this->response['data'] = $result;
        $this->response['error'] = null;

        return $this->response;
    }
}