<?php

class HandleSurat extends CI_Model{

	public function __construct() {
        parent::__construct();
        
        $this->load->database();

        /* variable for result which return in controller */
        $this->response = array();
    }

    public function findByNik($nik) {
        /* 
            in order to get same value with the web {http://localhost/kelurahan/index.php/surat/form/surat_ket_pengantar}    
            we shoud join 4 table (
                1. tweb_penduduk
                2. tweb_penduduk_agama
                3. tweb_penduduk_warganegara
                4. tweb_penduduk_pendidikan_kk
            )
        */

        $this->db->select('*');
        $this->db->from('tweb_penduduk');
        $this->db->join('tweb_penduduk_agama', 'tweb_penduduk.agama_id = tweb_penduduk_agama.id ');
        $this->db->join('tweb_penduduk_warganegara', 'tweb_penduduk.warganegara_id = tweb_penduduk_warganegara.id');
        $this->db->join('tweb_penduduk_pendidikan_kk', 'tweb_penduduk.pendidikan_kk_id = tweb_penduduk_pendidikan_kk.id');
        $this->db->where('nik', $nik);
        $result = $this->db->get()->result();

        if($result) {
            $this->response['code'] = 1;
            $this->response['msg']  = 'data ditemukan ';
            $this->response['data'] = $result;
            $this->response['error'] = null;
        } else {
            $this->response['code'] = 0;
            $this->response['msg']  = 'Mohon masukan data nik dengan benar';
            $this->response['data'] = $result;
            $this->response['error'] = 'data tidak ditemukan';
        }

        return $this->response;
        
    }

    public function save_surat_from_rt($data) {
        /* simpan new post from rt here*/
        return $this->db->insert('log_surat', $data);
    }


    public function search_bynik_for_new_letter($token) {
        
    }


}