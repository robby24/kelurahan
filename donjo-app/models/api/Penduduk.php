<?php
class Penduduk extends CI_Model{

	public function __construct() {
        parent::__construct();
        
        $this->load->database();
        $this->load->model('api/penduduk');
        $this->response = array();
    }

     public function tambah_warga($body) {
        if($this->user->checkTokenSame($body['t oken'])) {
            $data = array(
                'name' => $body['username'],
                'nik' => $body['nik'],
                'id_kk' => $body['id_kk'],
                'kk_level' => $body['kk_level']
            );            

            $data  = array( 
                'session_name()' => $body['username'],
                'username'      => $username, 
                'token_register'=> $token, 
                'active'        => 0 

            );
        } else {
            $this->response['code'] = 0;
            $this->response['msg']  = 'token tidak ada login untuk melanjutkan';
            $this->response['error'] = 'token tidak ada login untuk melanjutkan';
            $this->response['data']  = nsdull;          
        }
    }

    public function searchNik($nik) {
    	$this->db->select('*');
    	$this->db->from('tweb_penduduk');
    	$this->db->limit(20);
    	$result = $this->db->like('nik', $nik)->get()->result();

    	if(count($result) > 0 ) {
    		$this->response['code'] = 1;
	    	$this->response['msg']  = "data ditukan sebanyak " + count($result);
            $this->response['data']  = $result;          	
    	} else {
			$this->response['code'] = 1;
	    	$this->response['msg']  = 'data tidak ada!';
            $this->response['data']  = [];          	
    	}

    	return $this->response;

    }

}
    