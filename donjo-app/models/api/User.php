<?php
class User extends CI_Model{

	public function __construct() {
        parent::__construct();
        
        $this->load->database();
        $this->load->helper('password');
        $this->response = array();
    }
    
    public function checkTokenSame($token) {
        /*check session apakah dia ada atau tidak dalam sistem */

        $this->db->where(array('session'=> $token))->from('user');
        $result = $this->db->count_all_results();
        if($result > 0)  return true;
        else return false;

    }

    public function checkTokenSameAuth($token) {
        /*check session apakah dia ada atau tidak dalam sistem */

        $this->db->where(array('session'=> $token))->from('user');
        $result = $this->db->count_all_results();
        if($result > 0)  return true;
        else return false;

    }

    public function checkauth($username, $password) {

        $query  = array( 
            'id_grup'   => 4, 
            'username'  => $username, 
            'password'  => md5($password), 
            'active'    => 1
        );
        
        $result = $this->db->select('*')->from('user')->where($query)->get()->row();

        if($result) {

            /* generate token to save in databases*/
            $bahan = $result->username + $result->phone;

            /* GENERATE RANDOM TOKENS */
            $generateToken = $this->generatePasswordHash($bahan);

            $this->db->where(array('id' => $result->id));
            $token = $this->db->update('user', array('session' => $generateToken));

            $this->response['code']  = 1;
            $this->response['msg']   = 'anda berhasil login';
            $this->response['token'] = $generateToken;
            $this->response['error'] = null;
        } else {
            $this->response['code']  = 0;
            $this->response['msg']   = 'anda gagal login';
            $this->response['token'] = null;
            $this->response['error'] = 'password atau email tidak benar';
        }

        return $this->response;
    }


    /* aktifasi akun disini users melakukan aktifasi dengna memasukan username dan password token 
        yang diberikan oleh kelurahan 
    */
    public function registrasiAccount($username, $token) {
            $query  = array( 
                'id_grup'       => 4, 
                'username'      => $username, 
                'session'       => $token, 
                'active'        => 0 
            );
            
            $result = $this->db->select('*')->from('user')->where($query)->get()->row();

            if($result) {
                $this->response['user'] = $result->id;
                $this->response['code'] = 1;
                $this->response['msg']  = ['registrasi berhasil','mohon segera ganti password anda'];
                $this->response['error'] = null;
            } else {
                $this->response['code'] = 0;
                $this->response['msg']  = 'mohon masukan username dan password yang diberikan oleh petugas dengan benar';
                $this->response['error'] = 'proses registrasi awal gagal!';
            }

            return $this->response; 
    }

    public function changePasswordUserRt($id, $newPassword) {
        /* where to get field */
            $query  = array( 
                'id_grup' => 4, 
                'id' => $id,
                'active' => 0 
            );
        
           /* data batu */
            $dataChange = array(
                'active'    => 1,
                'password'  => md5($newPassword),
                'session'   => null /* for validation in the first step registrasi*/
            );
            
            $this->db->where($query);
            $this->db->update('user', $dataChange);
            $this->db->trans_complete(); 

            if($this->db->affected_rows()) {
                $this->response['code'] = 1;
                $this->response['msg']  = 'proses registrasi awal berhail';
                $this->response['error'] = null;
            } else {
                $this->response['code'] = 0;
                $this->response['msg']  = 'proses registrasi awal gagal';
                $this->response['error'] = ['gagal save data update'];
            }
       
        return $this->response;
    }


    private function generatePasswordHash($string) {

        $string = is_string($string) ? $string : strval($string);
        $pwHash = password_hash($string, PASSWORD_BCRYPT);

        if (password_needs_rehash($pwHash, PASSWORD_BCRYPT)) {
            $pwHash = password_hash($string, PASSWORD_BCRYPT);
        }

        return $pwHash;
    }

    /* destroy session token*/
    public function distroyTokenAuth($token) {
         if($this->user->checkTokenSame($token)) {

            $query = array('session' => $token);

            $this->db->where($query);
            $this->db->update('user', array('session' => null));

            return true;    

        } else {
            return false;

        }
        
    }

    public function getProfile($token) {
            /* khusus digunakan untuk mendapatkan data profile pegguna aplikasi */
            $this->db->select(['nik', 'phone','username', 'rt', 'user.foto', 'rw'])->from('tweb_penduduk');
            $this->db->join('user', 'user.nik_id=tweb_penduduk.nik');
            $this->db->join('tweb_wil_clusterdesa', 'tweb_wil_clusterdesa.id=tweb_penduduk.id_cluster');
            $this->db->where("user.session", $token);

            $result = $this->db->get()->row();

            if($result) {
                $this->response['code'] = 1;
                $this->response['msg']  = 'profile info';
                $this->response['error'] = null;
                $this->response['data']  = $result;

            } else {
                $this->response['code'] = 0;
                $this->response['msg']  = 'token tidak ada login untuk melanjutkan';
                $this->response['error'] = 'data tidak ada';
                $this->response['data']  = null;
            }

        return $this->response;
    }

    /* mengambil data seluruh warga di rt tertentu*/
    public function get_warga($token) {
        /* get data rt by token */
        $query = array('session' => $token);
        $this->db->select('*')->from('user')->where($query);
        $this->db->join('tweb_penduduk', 'tweb_penduduk.nik=user.nik_id');
        $this->db->join('tweb_wil_clusterdesa', 'tweb_wil_clusterdesa.id=tweb_penduduk.id_cluster');
            

        /* data kepala rt */
        $data_rt = $this->db->get()->row();
        
        /* cari warga dari table penduduk yang mana satu rt dengan pak kepala rt*/
        $this->db->select('*')->from('tweb_penduduk');
        $this->db->join('tweb_wil_clusterdesa', 'tweb_wil_clusterdesa.id=tweb_penduduk.id_cluster');

        $this->db->where(array('tweb_wil_clusterdesa.rt' => $data_rt->rt) );
        $result = $this->db->get()->result();


        if($result) {
            $this->response['code'] = 1;
            $this->response['msg']  = 'data ditemukan !';
            $this->response['error'] = null;
            $this->response['data']  = $result;

        } else {
            $this->response['code'] = 0;
            $this->response['msg']  = 'tidak ada data yang ditampilkan';
            $this->response['error'] = 'data tidak ada';
            $this->response['data']  = $data_rt;
        }

        return $this->response;

    }

    public function changeNomer($token, $no) {
        $this->db->set('phone', $no);
        $this->db->where('session', $token);
        $this->db->update('user');
        return true;
    }


   


}