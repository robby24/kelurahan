<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends Ci_controller {
    function __construct() {
        parent::__construct();
        $this->load->model('api/user', 'user');
        $incomingType = $_SERVER['CONTENT_TYPE'];
        if($incomingType != 'application/json' ) {
            header($_SERVER['SERVER_PROTOCOL'],  ' 500 INTERNAL SERVER ERROR');
            exit();
        }

        /*  get data as the json type  */
        $this->content = trim(file_get_contents('php://input'));
	}

    public function loginProcess() {
        

        $body    = json_decode($this->content, true);

        /* check falidation here */

        /*  data username dan password di lakukan sebagai token default yang diberikan 
            oleh kepala kelurahan pengguna harus masuk ke sistem dan kemudian mengganti password untuk 
            melanjutkan kesistem 

        */
        $result = $this->user->checkauth($body['username'], $body['password']);
        
        if($result['code'] == 1) {
            header('Content-Type', 'aplication/json');
            echo json_encode($result);

        } else {
            header('Content-Type', 'aplication/json');
            echo json_encode($result);
        }
    }

    
    public function logout() {
        $body = json_decode($this->content, true);
  
        /* distroy session before logout */
        $destroy = $this->user->distroyTokenAuth($body['token']);

        if($destroy) {
            /* send response to the client that logout success */
            header('Content-Type', 'aplication/json');
            echo json_encode(array(
                'code'  => 1,
                'msg'   =>     'logout berhasil'
            ));
    
        } else {
            header('Content-Type', 'aplication/json');
            echo json_encode(array(
                'code'  => 0,
                'msg'   => 'logout gagal token tidak cocok'
            ));
        }
        
    }


    public function registrasiAccount() {
        $body    = json_decode($this->content, true);
        /* 
            @ in this case we visualizasi that the mr tr  should activate her account before he can use his account 
            @ i simulate that mr rt were giving by somene in kelurahan with his name and password token random 
            
        */
        
        $result = $this->user->registrasiAccount($body['username'], $body['token']);
        if($result['code'] == 1 ) echo json_encode($result); 
        else echo json_encode($result); 
    }


    public function changeFirstPassword() {
        $body = json_decode($this->content, true);
        /* 
            change password default before continue to the dasboard menu
            this is the required step when mr rt want to continue or registerasi for the first step
        */

        $result = $this->user->changePasswordUserRt($body['id'], $body['newpassword']);

        header('Content-Type', 'aplication/json');
        echo json_encode($result);
    }


    public function getprofile() {
        $body = json_decode($this->content, true);
        $result = $this->user->getProfile($body['token']);

        header('Content-Type', 'aplication/json');
        echo json_encode($result);
    }


    public function AuthToken() {
         $body = json_decode($this->content, true);

         $result = $this->user->checkTokenSameAuth($body['token']);
         if($result) {

            echo json_encode(
                array(
                    'hasAcess' => true,
                    'login' => 'ya'
                )
            );
         }else {
            echo json_encode(
                array(
                    'hasAcess' => false,
                    'login' => 'no'
                )
            );
         }
    }

    
}