<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

class Menu_setting extends Ci_controller {
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('api/MenuSetting', 'menu');
        /* check session */
        $this->checkSession();

        /* THIS IS OPTIONS USED FOR TYPE ALLOWED POST OR GET SHOULD BE JSON TYPE  */
        $incomingType = $_SERVER['CONTENT_TYPE'];
        if($incomingType != 'application/json' ) {
            header($_SERVER['SERVER_PROTOCOL'],  ' 500 INTERNAL SERVER ERROR');
            exit();
        }

        /* variable for base result wich send to client */
        $this->response =  array();
        /*  get data as the json type  */
        $this->content  = trim(file_get_contents('php://input'));
    }
    
    public  function checkSession() {
        /*  check if session doenst exsists then send http 505 server eror  */
        if($this->session->userdata('id_user') == '') {
            header($_SERVER['SERVER_PROTOCOL'],  ' 500 INTERNAL SERVER ERROR YOU HAS NO LOGINED BEFFORE');
            die('login untuk melanjutkan');
        }        
    }


    public function getmenu() {
        $result = $this->menu->get_data();
        header('Content-Type', 'aplication/json');
        echo json_encode($result);
    }


}