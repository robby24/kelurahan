<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

class NewSurat extends Ci_controller {
    function __construct() {
        parent::__construct();
        $this->load->model('api/HandleSurat', 'HandleSurat');
        
        /* THIS IS OPTIONS USED FOR TYPE ALLOWED POST OR GET SHOULD BE JSON TYPE  */
        $incomingType = $_SERVER['CONTENT_TYPE'];
        if($incomingType != 'application/json' ) {
            header($_SERVER['SERVER_PROTOCOL'],  ' 500 INTERNAL SERVER ERROR');
            exit();
        }

        /* variable for base result wich send to client */
        $this->response = array();
        /*  get data as the json type  */
        $this->content  = trim(file_get_contents('php://input'));
    }
    

    public function SearchNikForNewSurat() {
        /* received data nik from client as a json type  */
        $body = json_decode($this->content, true);

        if($body['NIK'] != '') {
            $result = $this->HandleSurat->findByNik($body['NIK']);

            header('Content-Type', 'aplication/json');
            echo json_encode($result);

        } else {
            /* fill value to the variable base */
            $this->response['code'] = 0;
            $this->response['msg']  = 'valiabel \'NICK\' doesn\'t exsist';
            $this->response['data'] = null;
            $this->response['error'] = ['valiabel \'NICK\' doesn\'t exsist'];

            header('Content-Type', 'aplication/json');
            echo json_encode($this->response);
        } 

    }

    public function newSuratPengantar() {
        $body  = json_decode($this->content, true);
        $error = array();
        foreach($body as $key) {
            if($key == '') {
                array_push($error, "variabel".$key."tidak boleh kosong");
            }
        }

        if(count($err) > 0 ) {
            $result = array( 
                'code' => 0,
                'msg'  => "mohon isi semua field!",
                'data' => null,
                'error' => $error
            );
            /* send error result to the clients */
            header('Content-Type', 'aplication/json');
            echo json_encode($result);

        } else {
            /* if there is no eror continue to save data to databases  */
            $resultres =  $this->HandleSurat->createSuratPenganter($body);
            header('Content-Type', 'aplication/json');
            echo json_encode($resultres);
        }
        

    }
}