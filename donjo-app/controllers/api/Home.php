<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Ci_controller {
    function __construct() {
        parent::__construct();
        $this->load->model('api/Penduduk', 'penduduk');
        $this->load->model('api/User', 'user');
        $this->load->model('api/HandleSurat', 'surat');


        /* THIS IS OPTIONS USED FOR TYPE ALLOWED POST OR GET SHOULD BE JSON TYPE  */
       /* $incomingType = $_SERVER['CONTENT_TYPE'];
        if($incomingType != 'application/json' ) {
            header($_SERVER['SERVER_PROTOCOL'],  ' 500 INTERNAL SERVER ERROR');
            exit();
        }*/

        /* variable for base result wich send to client */
        $this->response =  array();
        /*  get data as the json type  */
        
        $this->content  = trim(file_get_contents('php://input'));
    }


    /* header data here*/
    public function get_info_rt() {
        if($this->hakAksestoken()) { 
            $body   = json_decode($this->content, true);    
            $token  = $this->getheaderToken();

            $result = $this->user->getProfile($token);
            header('Content-Type', 'aplication/json');
            echo json_encode($result);
        } else {
            die('403 forbiden token ');
        }


    }


    public function tambah_warga() {
        $body   = json_decode($this->content, true);
        $result = $this->user->tambah_warga($body);

        header('Content-Type', 'aplication/json');
        echo json_encode($result);

    }
    public function search_nik() {
        /*pencarian nik warga guna meembuat apliakisi*/
        if($this->hakAksestoken()) {
            $body   = json_decode($this->content, true);
            if(empty($body['nik'])) {
                 $nik = '';
            }

            $nik = $body['nik'];
            $result = $this->penduduk->searchNik($nik);

            header('Content-Type', 'aplication/json');
            echo json_encode($result);
   
        }  else {
            die('403 access forbiden');
        }
        

    }

    public function newsuratrt() {
        
        if($this->hakAksestoken()) {
            /*
                ambil data rt berdasarkan token yang dimiliki oleh rt 
                simpan data rt sebagai pengirim dari pnegirim data surat 
                lanjutkan dengan menyimpan data 
            */
            $body   = json_decode($this->content, true);
            $token  = $this->getheaderToken();
            $dataRt = $this->user->getProfile($token)['data'];

            $dataInput = array(
                'id_format_surat' => 1,//$body['id_format_surat'], /* id format surat */
                'id_pend'         => $body['id_pend'], /* surat dari warga yang melakukan pembuatan surat pengantart*/
                'id_pamong'       => null, /* penduduk null*/
                'id_user'         => $dataRt['nik'], // id rt yang melakukan pendatan
                'tanggal'         => date("Y-m-d"),
                'bulan'           => date('m'),
                'tahun'           => date('Y'),
                'no_surat'        => '',
                'lampiran'        => $body['keterangan'], /* keterangan yang dibuat oleh pak rt*/
                'nama_surat'      => null,
                'nik_non_warga'   => null,
                'nama_non_warga'  => null,
                'gambar_ktp'      => $body['gambar_ktp'], // gambar url ktp
                'gambar_kk'       => $body['gambar_kk'], // gambar url kk
            );


            die(print_r($body));
            // $this->surat->save_surat_from_rt($dataInput);     
            // $response = array(
            //     'msg'   => 'pembuatan lampiran sudah selesai',
            //     'code'  => 1,
            //     'error' => null, 
            // );

            // echo json_encode($response);

        } else {
            die('403 accesss forbidden!');
        }
    }

    /* get token authentication */
    private function getheaderToken() {
        $headers = apache_request_headers();
        if(isset($headers['authorization'])){
            $token = $headers['authorization'];
            return $token;
        } else {

            return false;


        }
    }
    /* check if ther is acceess to get data from databases */
    private function hakAksestoken() {
        $token = $this->getheaderToken();
        if($token) {
            $auth = $this->user->checkTokenSame($token);
            if(!$auth) {
                return false;
            } else {
                return true;
            }    
        } else {
            header('Content-Type', 'application/json');
            echo json_encode(array('msg' => 'token tidak vaild'));      
        } 
    }


    public function upload_file_image() {
        /*
            memproese penguploadan file image yang di kirim menggunakan json format 
            kemudian memparsing dan menggembalikan data upload url berdasarkan url foto tersebut 
        */

        if($this->hakAksestoken()) {

            $name = $this->input->post('imageName');
            $type = $this->input->post('typeImage');

            $uploadImage = $this->uploadImage('image', $name, $type);
            
            header('Content-Type', 'application/json');
            echo json_encode($uploadImage);         

        } else {
            // die('403 forbiden hak access');
            $uploadImage = array('msg' => 'upload image error');
            header('Content-Type', 'application/json');
            echo json_encode($uploadImage);            
        }
    }

    private function uploadImage($name, $filename, $typefile) {
        /*function for upload image only */
        $config['upload_path']          = './assets/files/dokumen';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['overwrite']            = true;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $temp = array();
        if ($this->upload->do_upload($name)) {
            $temp['success']      = true;
            $temp['path_image']   = $filename;
            $temp['message']      = "success to upload image to server";
        } else {
            $temp['success']      = false;
            $temp['path_image']   = null;
            $temp['message']      = "failed to upload image to server";
        }
        return $temp;
    }

    /* GET DATA WARGA YANG BERADA SATU RT DENGAN KEPALA RT*/
    public function get_all_penduduk() {
        if($this->hakAksestoken()){
            $token    = $this->getheaderToken();
            $result   = $this->user->get_warga($token);
            
            if(!$token) {
                die('failed');
            }
            header('Content-Type', 'application/json');
            echo json_encode($result);                

        } else { 
            die('403 access forbidden');
        }
    }

    /*home statistik in our app android */
    public function get_info() {
        if($this->hakAksestoken()){
            $body    = json_decode($this->content, true);
            $result  = $this->user->get_statistik_home($body);
        }
    }


    public function changeNomerRt() {
        if($this->hakAksestoken()) {
            $token  = $this->getheaderToken();
            $body   = json_decode($this->content, true);
            $result = $this->user->changeNomer($token, $body['nomer']);

            /* respon to the client */
            $res = array('msg' => 'nomer sudah diperbarui', 'code' => 1);

            
            header('Content-Type', 'application/json');
            echo json_encode($res);


        } else {
            die('access forbiden 403');
        }
    }

}


