<script type="text/javascript" src="<?= base_url()?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?= base_url()?>assets/js/validasi.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script>
	$(function ()
	{
		$('.select2').select2()
	});
</script>
<form action="<?php echo base_url(). 'mandiri/simpan_ajax_pin'; ?>" method="post" enctype="multipart/form-data">
	<div class='modal-body'>
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-danger">
					<div class="box-body">
						<input type="hidden" name="id_kepala" />
						<div class="form-group">
							<label for="nik">NIK / Nama RT </label>
							<select class="form-control input-sm select2 required"  id="nik" name="nama" style="width:100%;">
								<option value="">-- Silakan Cari Nama rt / </option>
								<?php foreach ($penduduk as $data): ?>
									<option value="<?php echo $data['nama']?>" data-idkepala="<?php echo $data['id_kepala']?>"><?php echo $data['nama']?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label class="control-label" for="pin">USERNAME</label>
							<input id="username" name="username" class="form-control input-sm" type="text" placeholder="Username">
						</div>
						<div class="form-group">
							<label class="control-label" for="pin">Password</label>
							<input id="password" name="password" class="form-control input-sm" type="password" placeholder="PIN Warga"></input>
							<p class="help-block">*) Jika Password tidak di isi maka sistem akan menghasilkan PIN secara acak.</p>
							<p class="help-block">**) 6 (enam) digit Angka.</p>
						</div>
						<div class="form-group">
								<label class="control-label" for="group">Group</label>
									<select class="form-control input-sm required" id="id_grup" name="id_grup">
										<option value="">-- Silakan Pilih Grup/ </option>
										<?php foreach ($user as $data): ?>
											<option value="<?php echo $data->id; ?>"> <?php echo $data->nama; ?></option>
										<?php endforeach; ?>
									</select>
							</div>
						<div class="form-group">
							<label class="control-label" for="phone">Phone</label>
							<input id="phone" name="phone" class="form-control input-sm" type="number" placeholder="No Telpon">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="reset" class="btn btn-social btn-flat btn-danger btn-sm" data-dismiss="modal"><i class='fa fa-sign-out'></i> Tutup</button>
			<button type="submit" class="btn btn-social btn-flat btn-info btn-sm" name="proses"><i class='fa fa-check'></i> Simpan</button>
		</div>
	</div>
</form>




<script>
	$(document).ready(function() {
		$("#nik").change(function() {
			var selected = $(this).find('option:selected');
       		var extra = selected.data('idkepala'); 
			$('input[name="id_kepala"]').val(extra);
		})

	})
</script>